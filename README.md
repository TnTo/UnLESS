# UnLESS: Unsupervised Learning for Electoral Systems' Studies

This software is presented in the `Presenting_UnLESS.pdf` paper, where its general functioning is described.

`exec_python.py` is the executable which launches the program.  
This launcher asks for a schedule to be executed: the `ex` folder includes some examples used in the paper.
`electoral_system.nlogo` includes the simulation written in NetLogo.<br>
`neural_network.py` includes the scripts used to manage the neural network and the learning.

The `*.ipynb` files report the data analysis done for the papers.<br>
The `data` folder includes the data produced and analyzed for the paper.<br>
Tho `plot` folder includes the plot used in paper to present the results.
